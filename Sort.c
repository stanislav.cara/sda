#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX 100

typedef struct {
    char nume[50];
    int varsta;
    float salariu;
} Persoana;

void afisarePersoane(Persoana persoane[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%-30s %-6d %-10.2f\n", persoane[i].nume, persoane[i].varsta, persoane[i].salariu);
    }
    printf("\n");
}

int citireDinFisier(Persoana persoane[]) {
    FILE* f = fopen("date.txt", "r");
    if (f == NULL) {
        perror("Eroare la deschiderea fisierului");
        exit(-1);
    }

    int n = 0;
    while (fscanf(f, "%49s %d %f", persoane[n].nume, &persoane[n].varsta, &persoane[n].salariu) == 3) {
        n++;
        if (n >= MAX) {
            printf("Numarul maxim de persoane a fost atins.\n");
            break;
        }
    }
    fclose(f);
    return n;
}

void inserareSort(Persoana persoane[], int n) {
    for (int i = 1; i < n; i++) {
        Persoana temp = persoane[i];
        int j = i - 1;
        while (j >= 0 && persoane[j].varsta > temp.varsta) {
            persoane[j + 1] = persoane[j];
            j--;
        }
        persoane[j + 1] = temp;
    }
}

void selectieSort(Persoana persoane[], int n) {
    for (int i = 0; i < n - 1; i++) {
        int minIdx = i;
        for (int j = i + 1; j < n; j++) {
            if (persoane[j].varsta < persoane[minIdx].varsta) {
                minIdx = j;
            }
        }
        if (minIdx != i) {
            Persoana temp = persoane[i];
            persoane[i] = persoane[minIdx];
            persoane[minIdx] = temp;
        }
    }
}

void bubleSort(Persoana persoane[], int n) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (persoane[j].varsta > persoane[j + 1].varsta) {
                Persoana temp = persoane[j];
                persoane[j] = persoane[j + 1];
                persoane[j + 1] = temp;
            }
        }
    }
}

void shakeSort(Persoana persoane[], int n) {
    int left = 0, right = n - 1;
    while (left < right) {
        for (int i = left; i < right; i++) {
            if (persoane[i].varsta > persoane[i + 1].varsta) {
                Persoana temp = persoane[i];
                persoane[i] = persoane[i + 1];
                persoane[i + 1] = temp;
            }
        }
        right--;
        for (int i = right; i > left; i--) {
            if (persoane[i].varsta < persoane[i - 1].varsta) {
                Persoana temp = persoane[i];
                persoane[i] = persoane[i - 1];
                persoane[i - 1] = temp;
            }
        }
        left++;
    }
}

void executaSortare(void (*sortFunction)(Persoana[], int), Persoana persoane[], int n, const char* numeSort) {
    Persoana persoaneCopie[MAX];
    memcpy(persoaneCopie, persoane, n * sizeof(Persoana));
    clock_t start = clock(); 
    sortFunction(persoaneCopie, n); 
    clock_t end = clock(); 
    printf("%s sortat:\n", numeSort);
    afisarePersoane(persoaneCopie, n);
    printf("Timp de executie pentru %s: %.6f secunde\n\n", numeSort, (double)(end - start) / CLOCKS_PER_SEC);
}


int main() {
    Persoana persoane[MAX];
    int n = citireDinFisier(persoane); 

    printf("Datele initiale:\n");
    afisarePersoane(persoane, n);

    executaSortare(inserareSort, persoane, n, "Insert Sort");
    executaSortare(selectieSort, persoane, n, "Select Sort");
    executaSortare(bubleSort, persoane, n, "Bubble Sort");
    executaSortare(shakeSort, persoane, n, "Shake Sort");

    return 0;
}
