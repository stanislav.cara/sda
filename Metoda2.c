#include <stdio.h>
#include <stdlib.h>

//Metoda 2: Coada implementata folosind o lista simpla inlantuita
typedef struct Node{
	int data;
	struct Node *next;
}Node;

typedef struct {
	Node *front, *rear;
}Queue;

void initQueue(Queue *q)
{
	q->front = NULL;
	q->rear = NULL;
}

int isEmpty(Queue *q)
{
	return q->front == NULL;
}

void enqueue(Queue *q, int n)
{
	Node* newNode = (Node *)malloc(sizeof(Node));

	if (!newNode)
	{
		printf("Alocare esuata\n");
		return;
	}
	newNode->data = n;
	newNode->next = NULL;

	if (isEmpty(q))
	{
		q->front = newNode;
	}
	else
	{
		q->rear->next = newNode;
	}

	q->rear = newNode;
	printf("Elementul %d adaugat in coada\n", n);
}

int dequeue(Queue *q)
{
	if (isEmpty(q))
	{
		printf("Coada este goala\n");
		return INT_MIN;
	}
	int element = q->front->data;
	Node *temp = q->front;
	q->front = q->front->next;

	if (q->front == NULL)
	{
		q->rear = NULL;
	}

	free(temp);
	return element;
}

void display(Queue *q)
{
	if (isEmpty(q))
	{
		printf("Coada este goala\n");
	}
	else
	{
		Node *temp = q->front;
		printf("Elementele cozii sunt: ");
		while (temp != NULL)
		{
			printf("%d ", temp->data);
			temp = temp->next;
		}
		printf("\n");
	}
}

int main()
{
	int n = 0, z = 0;
	Queue q;

	scanf("%d", &n);
	initQueue(&q);

	for (int i = 0; i < n; i++)
	{
		scanf("%d", &z);
		enqueue(&q, z);
		display(&q);
	}

	printf("Elementul eliminat: %d\n", dequeue(&q));
	display(&q);

	return 0;
}