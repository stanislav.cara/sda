#include <stdio.h>
#define TABLE_SIZE 10

int hashFunction(int number)
{
    return number % TABLE_SIZE;
}

void insertHash(int hashTable[], int number)
{
    int index = hashFunction(number);
    int originalIndex = index;

    while (hashTable[index] != -1)
    {
        index = (index + 1) % TABLE_SIZE;
        if (index == originalIndex)
        {
            printf("Tabelul este plin! Nu se poate insera %d.\n", number);
            return;
        }
    }

    hashTable[index] = number;
}

void printHashTable(int hashTable[])
{
    printf("Tabelul Hash:\n");
    for (int i = 0; i < TABLE_SIZE; i++)
    {
        if (hashTable[i] == -1)
            printf("[%d] -> NULL\n", i);
        else
            printf("[%d] -> %d\n", i, hashTable[i]);
    }
}

int main()
{
    int numbers[] = {22, 10, 14, 2, 12, 4, 11, 212};
    int n = sizeof(numbers) / sizeof(numbers[0]);

    int hashTable[TABLE_SIZE];
    for (int i = 0; i < TABLE_SIZE; i++)
    {
        hashTable[i] = -1;
    }

    for (int i = 0; i < n; i++)
    {
        insertHash(hashTable, numbers[i]);
    }

    printHashTable(hashTable);

    return 0;
}
