#include <stdio.h>
#include <limits.h>

#define MAX 100

//Metoda 1: Coada implementata folosind un vector
typedef struct {
	int items[MAX];
	int front, rear;
}Queue;

void initQueue(Queue *q)
{
	q->front = -1;
	q->rear = -1;
}

int isEmpty(Queue *q)
{
	return q->front == -1;
}

int isFull(Queue *q)
{
	return q->rear == MAX - 1;
}

void enqueue(Queue *q, int n)
{
	if (isFull(q))
	{
		printf("Coada este plina");
		return;
	}

	if (isEmpty(q))
	{
		q->front = 0;
	}

	q->rear++;
	q->items[q->rear] = n;
	printf("Elementul %d adaugat in coada\n", n);
}

int dequeue(Queue *q)
{
	if (isEmpty(q))
	{
		printf("Coada este goala\n");
		return INT_MIN;
	}
	int element = q->items[q->front];
	q->front++;
	if (q->front > q->rear)
	{
		q->front = q->rear = -1;
	}
	return element;
}

void display(Queue *q)
{
	if (isEmpty(q))
	{
		printf("Coada este goala\n");
	}
	else
	{
		printf("Elementele cozii sunt: ");
		for (int i = q->front; i <= q->rear; i++)
		{
			printf("%d ", q->items[i]);
		}
		printf("\n");
	}
}

int main()
{
	int n = 0, z = 0;
	Queue q;

	scanf("%d", &n);
	initQueue(&q);

	for (int i = 0; i < n; i++)
	{
		scanf("%d", &z);
		enqueue(&q, z);
		display(&q);
	}

	printf("Elementul eliminat: %d\n", dequeue(&q));
	display(&q);
	return 0;
}